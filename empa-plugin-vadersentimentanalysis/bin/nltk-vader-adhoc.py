from nltk.sentiment.vader import SentimentIntensityAnalyzer
sentences = [
    "Hi pretty! :-) how r u today?",
    "feeling sad today :-(",
    "Hi guys! Just went for a walk today! It's cold outside. Love u all! BTW I just made it to that job! ;-) see u there!",
    "Hello everybody! I was just thinking how to write a short sample of text for testing the language detector... Maybe I got it with this kind of writing! Or not?! Lets test it and see the results!"
]
sid = SentimentIntensityAnalyzer()
for sentence in sentences:
    print(sentence)
    ss = sid.polarity_scores(sentence)
    for k in sorted(ss):
        print('{0}: {1}\t'.format(k, ss[k]), end='')
    print()
