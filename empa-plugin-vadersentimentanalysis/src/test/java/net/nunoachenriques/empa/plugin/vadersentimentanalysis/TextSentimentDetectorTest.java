/* 
 * Copyright 2017 Nuno A. C. Henriques [nunoachenriques.net]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.nunoachenriques.empa.plugin.vadersentimentanalysis;

import net.nunoachenriques.empa.affective.text.HumanSentimentModel;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;
import java.util.Map;

import junitparams.FileParameters;
import junitparams.JUnitParamsRunner;
import junitparams.mappers.CsvWithHeaderMapper;

import static org.hamcrest.CoreMatchers.hasItems;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

/**
 * Testing the text sentiment detector functionality! Using <a
 * href="https://github.com/nunoachenriques/vader-sentiment-analysis"
 * target="_top">VADER Sentiment Analysis in Java</a>.
 *
 * @author Nuno A. C. Henriques [nunoachenriques.net]
 */
@RunWith(JUnitParamsRunner.class)
public class TextSentimentDetectorTest {

    private TextSentimentDetector tsd;

    /**
     * Instantiates local {@code TextSentimentDetector} object!
     */
    @Before
    public void init() {
        tsd = new TextSentimentDetector();
        assertNotNull("No TextSentimentDetector instantiated!", tsd);
    }

    /**
     * Test if languages given in a list are among the default available
     * languages.
     *
     * @param l Language (ISO 639-1 or ISO 639-3 code) from the list.
     */
    @Test
    @FileParameters(value = "classpath:net/nunoachenriques/empa/plugin/vadersentimentanalysis/languages.csv", mapper = CsvWithHeaderMapper.class)
    public void testGetLanguageAvailable(String l) {
        List<String> al = tsd.getAvailableLanguages();
        assertThat(al, hasItems(l));
    }

    /**
     * Compares the expected model {@code HumanSentimentModel.MODEL.POLARITY} to
     * the result from {@code getModel()}.
     */
    @Test
    public void testGetModelAvailable() {
        HumanSentimentModel expected = HumanSentimentModel.MODEL.POLARITY;
        HumanSentimentModel got = tsd.getModel();
        assertEquals("Model expected: [" + expected.toString() + "] got: [" + got + "]", expected, got);
    }

    /**
     * Test if the sentiment of english text samples is working by the package's
     * Sentiment Detector class. The testing is from some samples evaluated by
     * the original C.J. Hutto Python implementation improved by the NLTK module
     * team.
     *
     * @param c VADER sentiment polarity compound.
     * @param n VADER sentiment polarity negative.
     * @param l VADER sentiment polarity neutral.
     * @param p VADER sentiment polarity positive.
     * @param s The text sample.
     */
    @Test
    @FileParameters(value = "classpath:net/nunoachenriques/empa/plugin/vadersentimentanalysis/text-samples-en.csv", mapper = CsvWithHeaderMapper.class)
    public void testEnglishPolarity(Float c, Float n, Float l, Float p, String s) {
        Float sentiment = tsd.getSentiment(s, "en");
        assertEquals("Compound: [" + c.toString() + "] sample: [" + s + "] (polarity: " + n + "; " + l + "; " + p +")", c, sentiment);
    }

    /**
     * Test if the sentiment of english text samples is working by the package's
     * Sentiment Detector class. The testing is from some samples evaluated by
     * the original C.J. Hutto Python implementation improved by the NLTK module
     * team.
     *
     * @param c VADER sentiment polarity compound.
     * @param n VADER sentiment polarity negative.
     * @param l VADER sentiment polarity neutral.
     * @param p VADER sentiment polarity positive.
     * @param s The text sample.
     */
    @Test
    @FileParameters(value = "classpath:net/nunoachenriques/empa/plugin/vadersentimentanalysis/text-samples-en.csv", mapper = CsvWithHeaderMapper.class)
    public void testEnglishPolarityByClass(Float c, Float n, Float l, Float p, String s) {
        Map<HumanSentimentModel, Float> sentiment = tsd.getSentimentByClass(s, "en");
        assertEquals("Negative: [" + n.toString() + "] sample: [" + s + "] (compound: " + c.toString() + ")", n, sentiment.get(HumanSentimentModel.POLARITY.NEGATIVE));
        assertEquals("Neutral: [" + l.toString() + "] sample: [" + s + "] (compound: " + c.toString() + ")", l, sentiment.get(HumanSentimentModel.POLARITY.NEUTRAL));
        assertEquals("Positive: [" + p.toString() + "] sample: [" + s + "] (compound: " + c.toString() + ")", p, sentiment.get(HumanSentimentModel.POLARITY.POSITIVE));
    }
}
