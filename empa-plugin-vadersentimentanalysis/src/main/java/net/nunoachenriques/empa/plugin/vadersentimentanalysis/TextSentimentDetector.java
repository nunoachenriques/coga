/* 
 * Copyright 2017 Nuno A. C. Henriques [nunoachenriques.net]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.nunoachenriques.empa.plugin.vadersentimentanalysis;

import net.nunoachenriques.empa.affective.text.HumanSentimentIdentifier;
import net.nunoachenriques.empa.affective.text.HumanSentimentModel;
import net.nunoachenriques.vader.SentimentAnalysis;

import org.pmw.tinylog.Logger;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Implements the
 * {@link net.nunoachenriques.empa.affective.text.HumanSentimentIdentifier}
 * functionality, i.e., it's a plugin! It identifies the human sentiment of a
 * written text sample. Using <a
 * href="https://github.com/nunoachenriques/vader-sentiment-analysis"
 * target="_top">VADER Sentiment Analysis in Java</a>. <strong>Use
 * case</strong>:
 * <pre>
 * ...
 * <code>
 * import net.nunoachenriques.empa.plugin.vadersentimentanalysis.TextSentimentDetector
 * </code>
 * ...
 * <code>
 * TextSentimentDetector tsd = new TextSentimentDetector();
 * Float s1 = tsd.getSentiment("First text sample to identify sentiment! :-)", "en");
 * Float s2 = tsd.getSentiment("Second text sample to identify sentiment! :-(", "en");
 * </code>
 * ...
 * </pre>
 *
 * @author Nuno A. C. Henriques [nunoachenriques.net]
 */
public final class TextSentimentDetector
        implements HumanSentimentIdentifier {

    private static final SentimentAnalysis sentimentAnalysis;
    private static final List<String> languages; // Available languages (e.g., "en").
    static {
        sentimentAnalysis = new SentimentAnalysis();
        languages = sentimentAnalysis.getAvailableLanguages();
    }

    @SuppressWarnings("WeakerAccess")
    public TextSentimentDetector() {
    }

    @Override
    public List<String> getAvailableLanguages() {
        Logger.debug("Available languages: [{}]", languages.toString());
        return languages;
    }

    @Override
    public HumanSentimentModel getModel() {
        Logger.debug("Model: [{}]", HumanSentimentModel.MODEL.POLARITY.toString());
        return HumanSentimentModel.MODEL.POLARITY;
    }

    @Override
    public Float getSentiment(String sample, String language) {
        Float s_compound = null;
        if (sample == null) {
            Logger.warn("Sample IS NULL! Hint: sample must contain text!");
        } else if (language != null && languages.contains(language)) {
            s_compound = sentimentAnalysis.getSentimentAnalysis(sample, language).get("compound");
            Logger.debug("Sentiment: [{}] Sample: [{}] Language: [{}].", (s_compound == null ? null : s_compound.toString()), sample, language);
        } else {
            Logger.warn("Language [{}] NOT AVAILABLE! Hint: available languages [{}].", language, languages.toString());
        }
        return s_compound;
    }

    @Override
    public Map<HumanSentimentModel, Float> getSentimentByClass(String sample, String language) {
        Map<HumanSentimentModel, Float> s_classes = null;
        if (sample == null) {
            Logger.warn("Sample IS NULL! Hint: sample must contain text!");
        } else if (languages.contains(language)) {
            Map<String, Float> s = sentimentAnalysis.getSentimentAnalysis(sample, language);
            s_classes = new HashMap<>();
            s_classes.put(HumanSentimentModel.POLARITY.NEGATIVE, s.get("negative"));
            s_classes.put(HumanSentimentModel.POLARITY.NEUTRAL, s.get("neutral"));
            s_classes.put(HumanSentimentModel.POLARITY.POSITIVE, s.get("positive"));
            Logger.debug("Sentiment: [{}] Sample: [{}] Language: [{}].", s_classes.toString(), sample, language);
        } else {
            Logger.warn("Language [{}] not available! Hint: available languages [{}]", language, languages.toString());
        }
        return s_classes;
    }
}
