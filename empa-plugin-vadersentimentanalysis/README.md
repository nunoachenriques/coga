[![Apache 2.0 License](https://img.shields.io/badge/license-Apache%202.0-blue.svg "Apache 2.0 License")][1]

# VADER Sentiment Analysis EMPA Plugin

## Repository

https://gitlab.com/nunoachenriques/empa-plugin-vadersentimentanalysis

## Plugin

`net.nunoachenriques.empa.plugin.vadersentimentanalysis`

Written text human sentiment analysis plugin for
[EMPA](https://gitlab.com/nunoachenriques/coga/tree/master/empa) using
[VADER](https://github.com/nunoachenriques/vader-sentiment-analysis)
application.

The plugin announces itself with the text
`net.nunoachenriques.empa.plugin.vadersentimentanalysis.TextSentimentDetector`
in the file
`META-INF/services/net.nunoachenriques.empa.affective.text.HumanSentimentIdentifier`

## Install

Refer to the root project
[CogA](https://gitlab.com/nunoachenriques/coga/).

## Use cases

### As an [EMPA](https://gitlab.com/nunoachenriques/coga/tree/master/empa) plugin

The
[VADER Sentiment Analysis EMPA Plugin](https://gitlab.com/nunoachenriques/coga/tree/master/empa-plugin-vadersentimentanalysis)
distribution JAR must be in the classpath and has to be loaded by a
service provider class (as a `HumanSentimentIdentifier` implementation).

```java
HumanSentimentIdentifierService hsis = HumanSentimentIdentifierService.getInstance();
Float s1 = hsis.getSentiment("First text sample to identify sentiment! :-)", "en");
Float s2 = hsis.getSentiment("Second text sample to identify sentiment! :-(", "en");
```

### Stand alone

It may be used integrated with any other application class.

```java
import net.nunoachenriques.empa.plugin.vadersentimentanalysis.TextSentimentDetector
...
TextSentimentDetector tsd = new TextSentimentDetector();
Float s1 = tsd.getSentiment("First text sample to identify sentiment! :-)", "en");
Float s2 = tsd.getSentiment("Second text sample to identify sentiment! :-(", "en");
```

## License

Copyright 2017 Nuno A. C. Henriques [http://nunoachenriques.net/]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

[1]: http://www.apache.org/licenses/LICENSE-2.0.html
