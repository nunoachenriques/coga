[![Apache 2.0 License](https://img.shields.io/badge/license-Apache%202.0-blue.svg "Apache 2.0 License")][1]

# Optimaize Language Detector EMPA Plugin

## Repository

https://gitlab.com/nunoachenriques/coga/tree/master/empa-plugin-optimaizelanguagedetector

## Plugin

`net.nunoachenriques.empa.plugin.optimaizelanguagedetector`

Written text human language detector plugin for
[EMPA](https://gitlab.com/nunoachenriques/coga/tree/master/empa) using
[Optimaize Language Detector](https://github.com/optimaize/language-detector)
application.

The plugin announces itself with the text
`net.nunoachenriques.empa.plugin.optimaizelanguagedetector.TextLanguageDetector`
in the file
`META-INF/services/net.nunoachenriques.empa.language.HumanLanguageIdentifier`

The default languages to be loaded are in the file `textlanguagedetector.properties`.

## Install

Refer to the root project
[CogA](https://gitlab.com/nunoachenriques/coga/).

## Use cases

### As an [EMPA](https://gitlab.com/nunoachenriques/coga/tree/master/empa) plugin

The
[Optimaize Language Detector EMPA Plugin](https://gitlab.com/nunoachenriques/coga/tree/master/empa-plugin-optimaizelanguagedetector)
distribution JAR must be in the classpath and has to be loaded by a
service provider class (as a `HumanLanguageIdentifier` implementation).

```java
HumanLanguageIdentifierService hlis = HumanLanguageIdentifierService.getInstance();
hlis.setInUseLanguages(Arrays.asList("en", "pt"));
String ls1 = hlis.getLanguage("First text sample to identify language!"); // en
String ls2 = hlis.getLanguage("Segunda amostra de texto para identificar a linguagem!"); // pt
```

### Stand alone

It may be used integrated with any other application class.

```java
import net.nunoachenriques.empa.plugin.optimaizelanguagedetector.TextLanguageDetector
...
TextLanguageDetector tld = new TextLanguageDetector();
tld.setInUseLanguages(Arrays.asList("en", "pt"));
String ls1 = tld.getLanguage("First text sample to identify language!"); // en
String ls2 = tld.getLanguage("Segunda amostra de texto para identificar a linguagem!"); // pt
```

## License

Copyright 2017 Nuno A. C. Henriques [http://nunoachenriques.net/]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

[1]: http://www.apache.org/licenses/LICENSE-2.0.html
