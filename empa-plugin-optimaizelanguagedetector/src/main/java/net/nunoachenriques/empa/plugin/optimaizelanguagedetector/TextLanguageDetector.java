/* 
 * Copyright 2017 Nuno A. C. Henriques [nunoachenriques.net]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.nunoachenriques.empa.plugin.optimaizelanguagedetector;

import com.google.common.base.Optional;
import com.optimaize.langdetect.LanguageDetector;
import com.optimaize.langdetect.LanguageDetectorBuilder;
import com.optimaize.langdetect.i18n.LdLocale;
import com.optimaize.langdetect.ngram.NgramExtractors;
import com.optimaize.langdetect.profiles.BuiltInLanguages;
import com.optimaize.langdetect.profiles.LanguageProfile;
import com.optimaize.langdetect.profiles.LanguageProfileReader;
import com.optimaize.langdetect.text.CommonTextObjectFactories;
import com.optimaize.langdetect.text.TextObjectFactory;

import net.nunoachenriques.empa.language.HumanLanguageIdentifier;

import org.pmw.tinylog.Logger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

/**
 * <p>Implements the
 * {@link net.nunoachenriques.empa.language.HumanLanguageIdentifier}
 * functionality, i.e., it's a plugin! It identifies the known human language
 * of a written text sample. The list of in-use languages is given in the
 * properties file and automatically loaded.</p>
 *
 * <p>Using <a href="https://github.com/optimaize/language-detector"
 * target="_top">Language Detector from Optimaize</a>. Known caveats: when only
 * {@code en} and {@code pt} languages are loaded then the {@code pt} detection
 * works well even for short text samples. If similar languages are loaded then
 * some will be obscured by the others (e.g., {@code es} and {@code pt} then
 * {@code es} will obscure {@code pt}, i.e., a {@code pt} short text will be
 * detected as {@code es} or not at all, meaning, {@code null}). If many
 * languages are loaded, performance will degrade. Not thread-safe, should be
 * guaranteed by the service loader if needed.</p>
 *
 * <p><strong>Use case</strong>:</p>
 * <pre>
 * ...
 * <code>
 * import net.nunoachenriques.empa.plugin.optimaizelanguagedetector.TextLanguageDetector
 * </code>
 * ...
 * <code>
 * TextLanguageDetector tld = new TextLanguageDetector();
 * tld.setInUseLanguages(Arrays.asList("en","pt"));
 * String ls1 = tld.getLanguage("First text sample to identify language!");
 * String ls2 = tld.getLanguage("Second text sample to identify language!");
 * </code>
 * ...
 * </pre>
 *
 * @author Nuno A. C. Henriques [nunoachenriques.net]
 */
public final class TextLanguageDetector
        implements HumanLanguageIdentifier {

    private static final String PROPERTIES_FILE = "net/nunoachenriques/empa/plugin/optimaizelanguagedetector/textlanguagedetector.properties";
    private static Properties properties;
    /* Language Detector: languages' profile reader */
    private static List<LanguageProfile> languageProfiles;
    /* Language Detector: builder */
    private static LanguageDetector languageDetector;
    /* Language Detector: text object factory */
    private static TextObjectFactory textObjectFactory;

    /**
     * Loads properties (e.g., available languages) from a file and prepares
     * the detector for short clean texts with the loaded languages.
     *
     * @throws Exception If something failed (e.g., IOException on file access).
     */
    @SuppressWarnings("WeakerAccess")
    public TextLanguageDetector()
            throws Exception {
        loadProperties();
        List<String> languages = Arrays.asList(properties
                .getProperty("languages")
                .split("\\s*,\\s*"));
        setupLanguages(languages);
    }

    /**
     * Loads language detector properties (e.g., available languages) from a
     * specific package file.
     *
     * @throws Exception If something failed (e.g., IOException on file access).
     */
    private void loadProperties()
            throws Exception {
        try {
            properties = new Properties();
            properties.load(getClass().getClassLoader().getResourceAsStream(PROPERTIES_FILE));
        } catch (Exception e) {
            String m = "Unable to load properties from: " + PROPERTIES_FILE;
            Logger.error(m);
            throw new Exception(m, e.getCause());
        }
    }

    /**
     * Sets the languages available to be used for the detection process. Sets
     * the detector with the profiles. Instantiates the {@code textObject}.
     *
     * @param languages List of available languages in ISO 639-1 or 639-3
     *                  language code (e.g., en).
     * @throws Exception If {@code languages} is {@code null}, an empty list, or
     *                   not available by definition or by error.
     */
    private void setupLanguages(List<String> languages) throws Exception {
        languageProfiles = new LanguageProfileReader()
                .read(TextLanguageDetector.class.getClassLoader(),
                        "languages",
                        languages);
        languageDetector = LanguageDetectorBuilder
                .create(NgramExtractors.standard())
                .withProfiles(languageProfiles)
                .build();
        textObjectFactory = CommonTextObjectFactories
                .forDetectingShortCleanText();
    }

    @Override
    public List<String> getAvailableLanguages() {
        List<String> al = BuiltInLanguages.getShortTextLanguages();
        Logger.debug("Available languages: [{}]", al.toString());
        return al;
    }

    @Override
    public List<String> getInUseLanguages() {
        List<String> iul = new ArrayList<>();
        for (LanguageProfile lp : languageProfiles) {
            iul.add(lp.getLocale().getLanguage());
        }
        Logger.debug("In use languages: [{}]", iul.toString());
        return iul;
    }

    @Override
    public String getLanguage(String sample) {
        String l = null;
        if (sample == null) {
            Logger.warn("Sample IS NULL! Hint: sample must contain text!");
        } else {
            LdLocale ldl = getLocale(sample);
            l = (ldl != null ? ldl.getLanguage() : null);
            Logger.debug("Identified language: [{}] on: [{}]", l, sample);
        }
        return l;
    }

    @Override
    public void setInUseLanguages(List<String> languages)
            throws Exception {
        setupLanguages(languages);
        Logger.debug("Setup languages: [{}]", languages.toString());
    }

    /**
     * Gets the locale, i.e., a language, a region, from the text sample.
     *
     * @param sample Text to identify language.
     * @return A language detector locale object with data or {@code null}.
     * @see com.optimaize.langdetect.i18n.LdLocale
     */
    private LdLocale getLocale(String sample) {
        Optional<LdLocale> ldl = languageDetector
                .detect(textObjectFactory.forText(sample));
        return (ldl.isPresent() ? ldl.get() : null);
    }
}
