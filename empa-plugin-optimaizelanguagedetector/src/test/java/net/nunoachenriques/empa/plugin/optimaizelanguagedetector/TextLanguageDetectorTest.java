/* 
 * Copyright 2017 Nuno A. C. Henriques [nunoachenriques.net]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.nunoachenriques.empa.plugin.optimaizelanguagedetector;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Collections;
import java.util.List;

import junitparams.FileParameters;
import junitparams.JUnitParamsRunner;
import junitparams.mappers.CsvWithHeaderMapper;

import static org.hamcrest.CoreMatchers.hasItems;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

/**
 * Testing the text language detector functionality! Using <a
 * href="https://github.com/optimaize/language-detector"
 * target="_top">Language Detector from Optimaize</a>. Using CSV text
 * files (<code>.../resources/</code>).  Initial concept for the text
 * samples: micro type for texts less than ten words, micro type for
 * texts less than 140 characters (e.g., Twitter), and small type for
 * short texts bigger than the 140 characters. Additionally, some
 * samples are (p)ositive and others (n)egative or neutra(l).
 *
 * @author Nuno A. C. Henriques [nunoachenriques.net]
 */
@RunWith(JUnitParamsRunner.class)
public class TextLanguageDetectorTest {
    private TextLanguageDetector tld;

    /**
     * Instantiates local <code>TextLanguageDetector</code> object!
     */
    @Before
    public void textLanguageDetector() throws Exception {
        tld = new TextLanguageDetector();
        assertNotNull("No TextLanguageDetector instantiated!", tld);
    }

    /**
     * Test if languages given in a list are among the default available
     * languages.
     *
     * @param l Language (ISO 639-1 or ISO 639-3 code) from the list.
     */
    @Test
    @FileParameters(value = "classpath:net/nunoachenriques/empa/plugin/optimaizelanguagedetector/languages.csv", mapper = CsvWithHeaderMapper.class)
    public void getLanguageAvailable(String l) {
        List<String> al = tld.getAvailableLanguages();
        assertThat(al, hasItems(l));
    }

    /**
     * Test if languages given in a list are among the languages in use.
     *
     * @param l Language (ISO 639-1 or ISO 639-3 code) from the list.
     */
    @Test
    @FileParameters(value = "classpath:net/nunoachenriques/empa/plugin/optimaizelanguagedetector/languages.csv", mapper = CsvWithHeaderMapper.class)
    public void getLanguageInUse(String l) {
        List<String> iul = tld.getInUseLanguages();
        assertThat(iul, hasItems(l));
    }

    /**
     * Test if languages given in a list are among the available languages
     * to be set in use.
     *
     * @param l Language (ISO 639-1 or ISO 639-3 code) from the list.
     */
    @Test
    @FileParameters(value = "classpath:net/nunoachenriques/empa/plugin/optimaizelanguagedetector/languages.csv", mapper = CsvWithHeaderMapper.class)
    public void setLanguageInUse(String l) {
        try {
            tld.setInUseLanguages(Collections.singletonList(l));
            assertThat(tld.getInUseLanguages(), hasItems(l));
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    /**
     * Test if english text samples are detected as <code>en</code>
     * by the package's Language Detector class.
     *
     * @param t The text sample type (e.g., micro, tiny, small).
     * @param s The text sample.
     */
    @Test
    @FileParameters(value = "classpath:net/nunoachenriques/empa/plugin/optimaizelanguagedetector/text-samples-en.csv", mapper = CsvWithHeaderMapper.class)
    public void english(String t, String s) {
        String l = tld.getLanguage(s);
        assertEquals("Text type: ["+t+"] sample: ["+s+"]", "en", l);
    }

    /**
     * Test if portuguese text samples are detected as <code>pt</code>
     * by the package's Language Detector class.
     *
     * @param t The text sample type (e.g., micro, tiny, small).
     * @param s The text sample.
     */
    @Test
    @FileParameters(value = "classpath:net/nunoachenriques/empa/plugin/optimaizelanguagedetector/text-samples-pt.csv", mapper = CsvWithHeaderMapper.class)
    public void portuguese(String t, String s) {
        String l = tld.getLanguage(s);
        assertEquals("Text type: ["+t+"] sample: ["+s+"]", "pt", l);
    }
}
