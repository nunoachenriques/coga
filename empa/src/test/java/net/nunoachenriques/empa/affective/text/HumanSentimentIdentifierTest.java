/* 
 * Copyright 2016 Nuno A. C. Henriques [nunoachenriques.net]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.nunoachenriques.empa.affective.text;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import junitparams.FileParameters;
import junitparams.JUnitParamsRunner;
import junitparams.mappers.CsvWithHeaderMapper;

import static org.hamcrest.CoreMatchers.hasItems;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

/**
 * Testing the human language identifier functionality! It's a unit test for
 * the class, also an integration test for the plugin.
 *
 * @author Nuno A. C. Henriques [nunoachenriques.net]
 */
@RunWith(JUnitParamsRunner.class)
public final class HumanSentimentIdentifierTest {

    private static final HumanSentimentIdentifierService HSIS = HumanSentimentIdentifierService.getInstance();

    /**
     * Tests setting a specific implementation.
     */
    @BeforeClass
    public static void testSetAvailableImplementation() {
        HumanSentimentIdentifierService.setAvailableImplementation(net.nunoachenriques.empa.plugin.vadersentimentanalysis.TextSentimentDetector.class);
    }

    /**
     * Test of getAvailableLanguages method, of class HumanSentimentIdentifier.
     */
    @Test
    public void testGetAvailableLanguages() {
        String l = "en"; // TODO only English for now!
        List<String> al = HSIS.getAvailableLanguages();
        assertThat("Language [" + l + "] not available! Hint: available languages [" + al.toString() + "]", al, hasItems(l));

    }

    /**
     * Test of getModel method, of class HumanSentimentIdentifier.
     */
    @Test
    public void testGetModel() {
        boolean t = false;
        HumanSentimentModel m = HSIS.getModel();
        HumanSentimentModel.MODEL[] values = HumanSentimentModel.MODEL.values();
        for (HumanSentimentModel.MODEL v : values) {
            if (v == m) {
                t = true;
            }
        }
        assertTrue("Model [" + m.toString() + "] not found! Hint: models available [" + Arrays.toString(values) + "]", t);
    }

    /**
     * Test of getSentiment method, of class HumanSentimentIdentifier. Test if
     * the sentiment of english text samples is working. The samples were
     * evaluated by the original C.J. Hutto Python implementation
     * improved by the NLTK module team.
     *
     * @param c VADER sentiment polarity compound.
     * @param n VADER sentiment polarity negative.
     * @param l VADER sentiment polarity neutral.
     * @param p VADER sentiment polarity positive.
     * @param s The text sample.
     */
    @Test
    @FileParameters(value = "classpath:net/nunoachenriques/empa/affective/text/text-with-sentiment-samples-en.csv", mapper = CsvWithHeaderMapper.class)
    public void testGetSentimentEnglish(Float c, Float n, Float l, Float p, String s) {
        Float sentiment = HSIS.getSentiment(s, "en");
        assertEquals("Compound: [" + c.toString() + "] sample: [" + s + "] (polarity: " + n + "; " + l + "; " + p +")", c, sentiment);
    }

    /**
     * Test of getSentimentByClass method, of class HumanSentimentIdentifier.
     * Test if the sentiment of english text samples is working. The testing is
     * from some samples evaluated by the original C.J. Hutto Python
     * implementation improved by the NLTK module team.
     *
     * @param c VADER sentiment polarity compound.
     * @param n VADER sentiment polarity negative.
     * @param l VADER sentiment polarity neutral.
     * @param p VADER sentiment polarity positive.
     * @param s The text sample.
     */
    @Test
    @FileParameters(value = "classpath:net/nunoachenriques/empa/affective/text/text-with-sentiment-samples-en.csv", mapper = CsvWithHeaderMapper.class)
    public void testGetSentimentByClassEnglish(Float c, Float n, Float l, Float p, String s) {
        Map<HumanSentimentModel, Float> sentiment = HSIS.getSentimentByClass(s, "en");
        assertEquals("Negative: [" + n.toString() + "] sample: [" + s + "] (compound: " + c.toString() + ")", n, sentiment.get(HumanSentimentModel.POLARITY.NEGATIVE));
        assertEquals("Neutral: [" + l.toString() + "] sample: [" + s + "] (compound: " + c.toString() + ")", l, sentiment.get(HumanSentimentModel.POLARITY.NEUTRAL));
        assertEquals("Positive: [" + p.toString() + "] sample: [" + s + "] (compound: " + c.toString() + ")", p, sentiment.get(HumanSentimentModel.POLARITY.POSITIVE));
    }
}
