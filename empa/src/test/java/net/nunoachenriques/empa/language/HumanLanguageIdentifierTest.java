/* 
 * Copyright 2016 Nuno A. C. Henriques [nunoachenriques.net]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.nunoachenriques.empa.language;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Collections;
import java.util.List;

import junitparams.FileParameters;
import junitparams.JUnitParamsRunner;
import junitparams.mappers.CsvWithHeaderMapper;

import static org.hamcrest.CoreMatchers.hasItems;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

/**
 * Testing the human language identifier functionality! The human language
 * identifier service should load some implementation (e.g., default Optimaize
 * Language Detector EMPA plugin). Using CSV text files ({@code resources/...}).
 * Initial concept for the text samples: micro type for texts less than ten
 * words, micro type for texts less than 140 characters (e.g., Twitter), and
 * small type for short texts bigger than the 140 characters. Additionally, some
 * samples are (p)ositive and others (n)egative or neutra(l).
 *
 * @author Nuno A. C. Henriques [nunoachenriques.net]
 */
@RunWith(JUnitParamsRunner.class)
public final class HumanLanguageIdentifierTest {

    private static final HumanLanguageIdentifierService HLIS = HumanLanguageIdentifierService.getInstance();

    /**
     * Tests setting a specific implementation.
     */
    @BeforeClass
    public static void testSetAvailableImplementation() {
        HumanLanguageIdentifierService.setAvailableImplementation(net.nunoachenriques.empa.plugin.optimaizelanguagedetector.TextLanguageDetector.class);
    }

    /**
     * Tests if languages given in a list are among the default available
     * languages.
     *
     * @param l Language (ISO 639-1 or ISO 639-3 code) from the list.
     */
    @Test
    @FileParameters(value = "classpath:net/nunoachenriques/empa/language/languages.csv", mapper = CsvWithHeaderMapper.class)
    public void testGetAvailableLanguages(String l) {
        List<String> al = HLIS.getAvailableLanguages();
        assertThat("Available languages: [" + al.toString() + "] item: [" + l + "]", al, hasItems(l));
    }

    /**
     * Tests if languages given in a list are among the languages in use.
     *
     * @param l Language (ISO 639-1 or ISO 639-3 code) from the list.
     */
    @Test
    @FileParameters(value = "classpath:net/nunoachenriques/empa/language/languages.csv", mapper = CsvWithHeaderMapper.class)
    public void testGetInUseLanguages(String l) {
        List<String> iul = HLIS.getInUseLanguages();
        assertThat("In use languages: [" + iul.toString() + "] item: [" + l + "]", iul, hasItems(l));
    }

    /**
     * Tests if languages given in a list are among the available languages
     * to be set in use.
     *
     * @param l Language (ISO 639-1 or ISO 639-3 code) from the list.
     */
    @Test
    @FileParameters(value = "classpath:net/nunoachenriques/empa/language/languages.csv", mapper = CsvWithHeaderMapper.class)
    public void testSetInUseLanguages(String l) {
        try {
            HLIS.setInUseLanguages(Collections.singletonList(l));
            List<String> iul = HLIS.getInUseLanguages();
            assertThat("In use languages: [" + iul.toString() + "] item: [" + l + "]", iul, hasItems(l));
        } catch (Exception e) {
            fail(e.getMessage());
        } finally {
            try {
                HLIS.setInUseLanguages(HLIS.getAvailableLanguages());
            } catch (Exception ee) {
                fail(ee.getMessage());
            }
        }
    }

    /**
     * Test if english text samples are detected as <code>en</code> by the
     * package's Language Detector class.
     *
     * @param t The text sample type (e.g., micro, tiny, small).
     * @param s The text sample.
     */
    @Test
    @FileParameters(value = "classpath:net/nunoachenriques/empa/language/text-samples-en.csv", mapper = CsvWithHeaderMapper.class)
    public void testGetLanguageEnglish(String t, String s) {
        String l = HLIS.getLanguage(s);
        assertEquals("Text type: [" + t + "] sample: [" + s + "]", "en", l);
    }

    /**
     * Test if portuguese text samples are detected as <code>pt</code> by the
     * package's Language Detector class.
     *
     * @param t The text sample type (e.g., micro, tiny, small).
     * @param s The text sample.
     */
    @Test
    @FileParameters(value = "classpath:net/nunoachenriques/empa/language/text-samples-pt.csv", mapper = CsvWithHeaderMapper.class)
    public void testGetLanguagePortuguese(String t, String s) {
        String l = HLIS.getLanguage(s);
        assertEquals("Text type: [" + t + "] sample: [" + s + "]", "pt", l);
    }
}
