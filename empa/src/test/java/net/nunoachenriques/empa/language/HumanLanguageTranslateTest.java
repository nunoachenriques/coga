/* 
 * Copyright 2016 Nuno A. C. Henriques [nunoachenriques.net]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.nunoachenriques.empa.language;

import junitparams.JUnitParamsRunner;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.pmw.tinylog.Configurator;
import org.pmw.tinylog.Level;

import java.io.File;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Testing the human language translate service.
 *
 * @author Nuno A. C. Henriques [nunoachenriques.net]
 */
@RunWith(JUnitParamsRunner.class)
public final class HumanLanguageTranslateTest {

    private static final HumanLanguageTranslateService HLTS = HumanLanguageTranslateService.getInstance();

    /**
     * Tests setting a specific implementation.
     */
    @BeforeClass
    public static void testSetAvailableImplementation() {
        Configurator.defaultConfig().level(Level.DEBUG).activate();
        HumanLanguageTranslateService.setAvailableImplementation(net.nunoachenriques.empa.plugin.apertiumlanguagetranslator.TextLanguageTranslator.class);
    }

    /**
     * Tests setup!
     */
    @Test
    public void testSetup() {
        try {
            HumanLanguageTranslateService hlts = HumanLanguageTranslateService.getInstance();
            hlts.setup(new File(System.getProperty("java.io.tmpdir")));
            assertNotNull("FAILED to SETUP!", hlts);
        } catch (Exception e) {
            fail();
        }
    }

    /**
     * Tests if there is at least one language pair available!
     */
    @Test
    public void testGetAvailableLanguagePairs() throws Exception {
        HLTS.setup(new File(System.getProperty("java.io.tmpdir")));
        List alp = HLTS.getAvailableLanguagePairs();
        assertTrue("No language pairs available!", alp.size() > 0);
    }

    /**
     * Tests if it translates!
     */
    @Test
    public void testGetTranslation() {
        try {
            HLTS.setup(new File(System.getProperty("java.io.tmpdir")));
            assertEquals("Translation FAILED! pt-gl Olá -> Ola", "Ola", HLTS.getTranslation("Olá", "pt", "gl"));
            assertEquals("Translation FAILED! en-gl Hello -> Ola", "Ola", HLTS.getTranslation("Hello", "en", "gl"));
        } catch (Exception e) {
            fail();
        }
    }
}
