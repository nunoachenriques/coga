/*
 * Copyright 2018 Nuno A. C. Henriques [nunoachenriques.net]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.nunoachenriques.empa;

import junitparams.FileParameters;
import junitparams.JUnitParamsRunner;
import junitparams.mappers.CsvWithHeaderMapper;
import net.nunoachenriques.empa.affective.text.HumanSentimentIdentifierService;
import net.nunoachenriques.empa.language.HumanLanguageIdentifierService;
import net.nunoachenriques.empa.language.HumanLanguageTranslateService;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.File;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * The human sentiment test class which uses services from language (e.g.,
 * language identifier) and affective packages (e.g., text sentiment identifier)
 * to integrate and serve some useful results.
 *
 * @author Nuno A. C. Henriques [nunoachenriques.net]
 */
@RunWith(JUnitParamsRunner.class)
public final class AffectiveLanguageIntegrationTest {

    private static final HumanLanguageIdentifierService languageService = HumanLanguageIdentifierService.getInstance();
    private static final HumanLanguageTranslateService translateService = HumanLanguageTranslateService.getInstance();
    private static final HumanSentimentIdentifierService sentimentService = HumanSentimentIdentifierService.getInstance();

    /**
     * Tests setting up.
     */
    @BeforeClass
    public static void testSetup() {
        try {
            translateService.setup(new File(System.getProperty("java.io.tmpdir")));
        } catch (Exception e) {
            fail("Translate service setup FAILED! " + e.getMessage());
        }
    }

    @Test
    @FileParameters(value = "classpath:net/nunoachenriques/empa/polarity-text-samples.csv", mapper = CsvWithHeaderMapper.class)
    public void testGetSentiment(Float sentimentExpected, String s) {
        Float sentimentGot = getSentiment(s);
        assertEquals("Sample: [" + s + "]", sentimentExpected, sentimentGot);
    }

    /**
     * Gets the polarity of the text sample sentiment analysis. Moreover, it
     * translates from the detected language to the one accepted by the
     * sentiment analyzer. Additionally, deals with only ASCII emoticons. All in
     * a best effort to get the sentiment.
     *
     * @param text The sample to get the sentiment from.
     * @return Polarity [-1, 1].
     */
    private Float getSentiment(String text) {
        try {
            List<String> ssal = sentimentService.getAvailableLanguages();
            String ssl = ssal.get(0); // The first is the canonical one, i.e., "en"
            Pattern emoticonOnlyPattern = Pattern.compile("([>}])?([:;=x8#%])(')?([-o])?([)(/\\\\*&#$bDPpSsxX])"); // https://en.wikipedia.org/wiki/List_of_emoticons
            Matcher emoticonOnlyMatcher = emoticonOnlyPattern.matcher(text);
            boolean emoticonOnly = emoticonOnlyMatcher.matches();
            String identifiedLanguage = emoticonOnly ? ssl : languageService.getLanguage(text); // 2-char ISO 639-1 or 639-3
            Float sentiment;
            if (identifiedLanguage != null) { // Go for sentiment analysis
                if (!ssal.contains(identifiedLanguage)) { // Try to translate to a known language for the sentiment analysis
                    try {
                        String t;
                        if (identifiedLanguage.equals("pt")) { // TODO Fix this special hardcoded case in a general one
                            /*
                             * A pivot language pt-gl-en is needed because there's no pt-en in Apertium (empa-plugin-apertiumlanguagetranslator)
                             */
                            t = translateService.getTranslation(translateService.getTranslation(text, identifiedLanguage, "gl"), "gl", ssl);
                        } else { // Try to translate in general
                            t = translateService.getTranslation(text, identifiedLanguage, ssl);
                        }
                        sentiment = sentimentService.getSentiment(t, ssl);
                    } catch (Exception e) { // Translation failed
                        throw new AssertionError("Translation from [" + identifiedLanguage + "] to [" + ssl + "] FAILED!", e);
                    }
                } else {
                    sentiment = sentimentService.getSentiment(text, identifiedLanguage);
                }
            } else if (emoticonOnlyMatcher.find()) {
                /*
                 * If neither emoticon only nor language identified and
                 * text has mixed emoticons then get sentiment only on them.
                 */
                sentiment = sentimentService.getSentiment(emoticonOnlyMatcher.group(), ssl);
            } else {
                throw new AssertionError("Language NOT identified for [" + text + "] | Languages: " + languageService.getInUseLanguages().toString());
            }
            if (sentiment == null) {
                throw new AssertionError("Sentiment FAILED to analyze for [" + text + "]");
            }
            return sentiment;
        } catch (Exception e) {
            return null;
        }
    }
}
