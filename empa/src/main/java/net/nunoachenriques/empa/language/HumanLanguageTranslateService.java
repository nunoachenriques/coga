/* 
 * Copyright 2017 Nuno A. C. Henriques [nunoachenriques.net]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.nunoachenriques.empa.language;

import org.pmw.tinylog.Logger;

import java.io.File;
import java.util.List;
import java.util.ServiceLoader;

/**
 * The human language translate service provider! The constructor it's private
 * and uses a service loader for the interface class! It's use depends on an
 * available implementation, i.e.,
 * {@code HumanLanguageTranslateService hlts = HumanLanguageTranslateService.getInstance();}
 * will auto discovery {@link #setAvailableImplementation(Class)}.
 *
 * @author Nuno A. C. Henriques [nunoachenriques.net]
 * @see HumanLanguageTranslate
 */
public final class HumanLanguageTranslateService {

    private static ServiceLoader<HumanLanguageTranslate> loader;
    private static HumanLanguageTranslateService service;
    private static HumanLanguageTranslate implementation;

    private HumanLanguageTranslateService() {
        loader = ServiceLoader.load(HumanLanguageTranslate.class);
    }

    /**
     * Gets an instance of this class. If no single instance is available, i.e.,
     * {@code null}, then creates and returns a single new one.
     *
     * @return The single instance of this class, the service provider.
     * @throws ExceptionInInitializerError On failure to get a service implementation!
     */
    public static synchronized HumanLanguageTranslateService getInstance()
            throws ExceptionInInitializerError {
        if (service == null) {
            service = new HumanLanguageTranslateService();
        }
        setAvailableImplementation(null);
        return service;
    }

    /**
     * Sets a specific implementation available
     * (e.g., {@code net.nunoachenriques.empa.plugin.apertium.TextLanguageTranslate.class}).
     * If {@code null} is passed then it searches for an implementation of the
     * {@link HumanLanguageTranslate} service. The search for an
     * implementation is made over the list auto constructed by the several
     * plugins announcements (e.g., in each plugin's package file
     * {@code META-INF/services/net.nunoachenriques.empa.language.HumanLanguageTranslate}
     * with the content of the plugin's implementation class such as
     * {@code net.nunoachenriques.empa.plugin.apertium.TextLanguageTranslate}).
     * Loads the first one to be found.
     *
     * @param c A Java class name of a specific implementation to pick, {@code null} for auto discovery.
     * @throws ExceptionInInitializerError On failure to get a service implementation!
     */
    @SuppressWarnings("WeakerAccess")
    public static synchronized void setAvailableImplementation(Class c)
            throws ExceptionInInitializerError {
        implementation = null;
        try {
            loader.reload();
            if (c == null) {
                implementation = loader.iterator().next();
            } else {
                for (HumanLanguageTranslate hlt : loader) {
                    if (c.equals(hlt.getClass())) {
                        implementation = hlt;
                        break;
                    }
                }
            }
            Logger.debug("Implementation: [{}]", implementation.toString());
        } catch (Exception e) {
            Logger.error("Implementation not loaded! {}", e.getMessage());
            throw new ExceptionInInitializerError(e);
        }
    }

    /**
     * @see HumanLanguageTranslate#setup(File)
     */
    public void setup(File baseDir) throws Exception {
        implementation.setup(baseDir);
    }

    /**
     * @see HumanLanguageTranslate#getAvailableLanguagePairs()
     */
    public List getAvailableLanguagePairs() {
        return implementation.getAvailableLanguagePairs();
    }

    /**
     * @see HumanLanguageTranslate#getTranslation(String, String, String)
     */
    public String getTranslation(String sample, String from, String to) throws Exception {
        return implementation.getTranslation(sample, from, to);
    }
}
