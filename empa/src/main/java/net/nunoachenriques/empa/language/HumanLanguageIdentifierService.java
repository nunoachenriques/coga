/* 
 * Copyright 2017 Nuno A. C. Henriques [nunoachenriques.net]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.nunoachenriques.empa.language;

import org.pmw.tinylog.Logger;

import java.util.List;
import java.util.ServiceLoader;

/**
 * The human language identifier service provider! The constructor it's private
 * and uses a service loader for the interface class! It's use depends on an
 * available implementation, i.e.,
 * {@code HumanLanguageIdentifierService hlis = HumanLanguageIdentifierService.getInstance();}
 * will auto discovery {@link #setAvailableImplementation(Class)}.
 *
 * @author Nuno A. C. Henriques [nunoachenriques.net]
 * @see HumanLanguageIdentifier
 */
public final class HumanLanguageIdentifierService {

    private static ServiceLoader<HumanLanguageIdentifier> loader;
    private static HumanLanguageIdentifierService service;
    private static HumanLanguageIdentifier implementation;

    private HumanLanguageIdentifierService() {
        loader = ServiceLoader.load(HumanLanguageIdentifier.class);
    }

    /**
     * Gets an instance of this class. If no single instance is available, i.e.,
     * {@code null}, then creates and returns a single new one.
     *
     * @return The single instance of this class, the service provider.
     * @throws ExceptionInInitializerError On failure to get a service implementation!
     */
    public static synchronized HumanLanguageIdentifierService getInstance()
            throws ExceptionInInitializerError {
        if (service == null) {
            service = new HumanLanguageIdentifierService();
        }
        setAvailableImplementation(null);
        return service;
    }

    /**
     * Sets a specific implementation available
     * (e.g., {@code net.nunoachenriques.empa.plugin.optimaizelanguagedetector.TextLanguageDetector.class}).
     * If {@code null} is passed then it searches for an implementation of the
     * {@link HumanLanguageIdentifier} service. The search for an
     * implementation is made over the list auto constructed by the several
     * plugins announcements (e.g., in each plugin's package file
     * {@code META-INF/services/net.nunoachenriques.empa.language.HumanLanguageIdentifier}
     * with the content of the plugin's implementation class such as
     * {@code net.nunoachenriques.empa.plugin.optimaizelanguagedetector.TextLanguageDetector}).
     * Loads the first one to be found.
     *
     * @param c A Java class name of a specific implementation to pick, {@code null} for auto discovery.
     * @throws ExceptionInInitializerError On failure to get a service implementation!
     */
    public static synchronized void setAvailableImplementation(Class c)
            throws ExceptionInInitializerError {
        implementation = null;
        try {
            loader.reload();
            if (c == null) {
                implementation = loader.iterator().next();
            } else {
                for (HumanLanguageIdentifier hli : loader) {
                    if (c.equals(hli.getClass())) {
                        implementation = hli;
                        break;
                    }
                }
            }
            Logger.debug("Implementation: [{}]", implementation.toString());
        } catch (Exception e) {
            Logger.error("Implementation not loaded! {}", e.getMessage());
            throw new ExceptionInInitializerError(e);
        }
    }

    /**
     * @see net.nunoachenriques.empa.language.HumanLanguageIdentifier#getAvailableLanguages()
     */
    public List<String> getAvailableLanguages() {
        return implementation.getAvailableLanguages();
    }

    /**
     * @see net.nunoachenriques.empa.language.HumanLanguageIdentifier#getInUseLanguages()
     */
    public List<String> getInUseLanguages() {
        return implementation.getInUseLanguages();
    }

    /**
     * @see net.nunoachenriques.empa.language.HumanLanguageIdentifier#getLanguage(String)
     */
    public String getLanguage(String sample) {
        return implementation.getLanguage(sample);
    }

    /**
     * @see net.nunoachenriques.empa.language.HumanLanguageIdentifier#setInUseLanguages(List)
     */
    public void setInUseLanguages(List<String> languages) throws Exception {
        implementation.setInUseLanguages(languages);
    }
}
