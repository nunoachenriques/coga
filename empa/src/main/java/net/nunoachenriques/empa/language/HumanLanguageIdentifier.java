/* 
 * Copyright 2017 Nuno A. C. Henriques [nunoachenriques.net]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.nunoachenriques.empa.language;

import java.util.List;

/**
 * The human language identifier functionality specification! It's purpose is
 * to identify the known human language of a written text sample. Use case:
 * <pre>
 * ...
 * <code>
 * HumanLanguageIdentifier hli = HumanLanguageIdentifierService.getInstance();
 * hli.setInUseLanguages(Arrays.asList("en","pt"));
 * String l1 = hli.getLanguage("First text sample to identify language!");
 * String l2 = hli.getLanguage("Segunda amostra de texto para identificar a linguagem!");
 * </code>
 * ...
 * </pre>
 *
 * @author Nuno A. C. Henriques [nunoachenriques.net]
 */
public interface HumanLanguageIdentifier {

    /**
     * Gets the languages available to be set for the detection process.
     *
     * @return List of available languages in ISO 639-1 or 639-3 language code
     * (e.g., en). {@code null} or empty list if none.
     */
    List<String> getAvailableLanguages();

    /**
     * Gets the languages in use for the detection process.
     *
     * @return List of available languages in ISO 639-1 or 639-3 language code
     * (e.g., en). {@code null} or empty list if none.
     */
    List<String> getInUseLanguages();

    /**
     * Gets the identified language from the text sample.
     *
     * @param sample Text sample.
     * @return ISO 639-1 or 639-3 language code (e.g., en). {@code null} if
     * none.
     */
    String getLanguage(String sample);

    /**
     * Sets the languages available to be used for the detection process.
     *
     * @param languages List of available languages in ISO 639-1 or 639-3
     * language code (e.g., en).
     * @throws Exception If {@code languages} is {@code null}, an empty list, or
     * not available by definition or by error.
     */
    void setInUseLanguages(List<String> languages) throws Exception;
}
