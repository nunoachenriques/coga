/* 
 * Copyright 2018 Nuno A. C. Henriques [nunoachenriques.net]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.nunoachenriques.empa.language;

import java.io.File;
import java.util.List;
import java.util.Map;

/**
 * The human language translate functionality specification! It's purpose is
 * to translate between a pair of human languages, one source
 * (of a written text sample) and one target. Use case:
 * <pre>
 * ...
 * <code>
 * HumanLanguageTranslateService hlts = HumanLanguageTranslateService.getInstance();
 * hlts.setup(new File(System.getProperty("java.io.tmpdir")));
 * String t1 = hlts.getTranslation("hello", "en", "gl");
 * String t2 = hlts.getTranslation("ola", "gl", "en");
 * </code>
 * ...
 * </pre>
 *
 * @author Nuno A. C. Henriques [nunoachenriques.net]
 */
public interface HumanLanguageTranslate {

    /**
     * Sets the directory to use as root for all the (cached) language packages.
     * Installs and loads available language packages.
     *
     * @param baseDir Base directory for all cached files (e.g., language packages).
     */
    void setup(File baseDir) throws Exception;

    /**
     * Gets the language pairs available for the translation process.
     *
     * @return List of available language pairs in ISO 639-1 or 639-3 language
     * code (e.g., en). Empty list if none.
     */
    List<Map.Entry<String, String>> getAvailableLanguagePairs();

    /**
     * Gets the text sample translation from one language to the other.
     *
     * @param sample Text sample to translate.
     * @param from Text sample ISO 639-1 or 639-3 language code (e.g., en).
     * @param to Translation ISO 639-1 or 639-3 language code (e.g., en).
     * @return Text sample translation.
     * @throws Exception if the language pair ({@code from}, {@code to}) is unavailable or some other failure.
     */
    String getTranslation(String sample, String from, String to) throws Exception;
}
