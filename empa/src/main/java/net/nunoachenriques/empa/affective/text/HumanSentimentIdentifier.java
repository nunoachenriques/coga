/* 
 * Copyright 2017 Nuno A. C. Henriques [nunoachenriques.net]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.nunoachenriques.empa.affective.text;

import java.util.List;
import java.util.Map;

/**
 * The human sentiment identifier functionality specification! It's purpose is
 * to identify the known human sentiment of a written text sample. Use case:
 * <pre>
 * ...
 * <code>
 * HumanSentimentIdentifierService hsis = HumanSentimentIdentifierService.getInstance();
 * Float s1 = hsis.getSentiment("First text sample to identify sentiment! :-)", "en");
 * Float s2 = hsis.getSentiment("Segunda amostra de texto para identificar o sentimento!", "pt");
 * </code>
 * ...
 * </pre>
 *
 * @author Nuno A. C. Henriques [nunoachenriques.net]
 */
public interface HumanSentimentIdentifier {

    /**
     * Gets the languages available for the sentiment analysis process.
     *
     * @return List of available languages in ISO 639-1 or 639-3 language code
     * (e.g., en). {@code null} or empty list if none or language agnostic.
     */
    List<String> getAvailableLanguages();

    /**
     * Gets the supported model for the values by classes.
     *
     * @return Which of the supported models are the values for.
     * @see #getSentimentByClass(java.lang.String, java.lang.String)
     * @see HumanSentimentModel
     */
    HumanSentimentModel getModel();

    /**
     * Gets the calculated sentiment polarity from the text sample.
     *
     * @param sample Text sample to analyze.
     * @param language ISO 639-1 or 639-3 sentiment code (e.g., en).
     * @return A compound value with sentiment polarity normalized between
     * [-1, 1], i.e., -1.0 meaning the most negative sentiment and 1.0
     * representing the most positive, 0.0 for neutral. {@code null} on failure.
     */
    Float getSentiment(String sample, String language);

    /**
     * Gets all individual classes polarity of the calculated sentiment from the
     * text sample.
     *
     * @param sample Text sample to analyze.
     * @param language ISO 639-1 or 639-3 sentiment code (e.g., en).
     * @return A map with sentiment class name and value pairs. Examples
     * depending on the implemented model:
     * [(HumanSentimentModel.POLARITY.NEGATIVE, 0.0),
     * (HumanSentimentModel.POLARITY.NEUTRAL, 0.1),
     * (HumanSentimentModel.POLARITY.POSITIVE, 0.7)] in a polarity model, or
     * [(HumanSentimentModel.VAD.VALENCE, 0.4),
     * (HumanSentimentModel.VAD.AROUSAL, 0.6),
     * (HumanSentimentModel.VAD.DOMINANCE, 0.1)] in a Valence, Arousal, Dominance
     * (VAD) model. Values must be normalized: [0, 1]. {@code null} on failure.
     * @see #getModel()
     */
    Map<HumanSentimentModel, Float> getSentimentByClass(String sample, String language);
}
