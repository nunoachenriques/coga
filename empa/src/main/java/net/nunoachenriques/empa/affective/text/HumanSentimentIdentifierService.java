/* 
 * Copyright 2017 Nuno A. C. Henriques [nunoachenriques.net]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.nunoachenriques.empa.affective.text;

import org.pmw.tinylog.Logger;

import java.util.List;
import java.util.Map;
import java.util.ServiceLoader;

/**
 * The human sentiment identifier service provider! The constructor it's private
 * and uses a service loader for the interface class! It's use depends on an
 * available implementation, i.e.,
 * {@code HumanSentimentIdentifierService hsis = HumanSentimentIdentifierService.getInstance();}
 * will auto discovery {@link #setAvailableImplementation(Class)}.
 *
 * @author Nuno A. C. Henriques [nunoachenriques.net]
 * @see HumanSentimentIdentifier
 */
public final class HumanSentimentIdentifierService {

    private static ServiceLoader<HumanSentimentIdentifier> loader;
    private static HumanSentimentIdentifierService service;
    private static HumanSentimentIdentifier implementation;

    private HumanSentimentIdentifierService() {
        loader = ServiceLoader.load(HumanSentimentIdentifier.class);
    }

    /**
     * Gets an instance of this class. If no single instance is available, i.e.,
     * {@code null}, then creates and returns a single new one.
     *
     * @return The single instance of this class, the service provider.
     * @throws ExceptionInInitializerError On failure to get a service implementation!
     */
    public static synchronized HumanSentimentIdentifierService getInstance()
            throws ExceptionInInitializerError {
        if (service == null) {
            service = new HumanSentimentIdentifierService();
        }
        setAvailableImplementation(null);
        return service;
    }

    /**
     * Sets a specific implementation available
     * (e.g., {@code net.nunoachenriques.empa.plugin.vadersentimentanalysis.TextSentimentDetector.class}).
     * If {@code null} is passed then it searches for an implementation of the
     * {@link HumanSentimentIdentifier} service. The search for an
     * implementation is made over the list auto constructed by the several
     * plugins announcements (e.g., in each plugin's package file
     * {@code META-INF/services/net.nunoachenriques.empa.affective.text.HumanSentimentIdentifier}
     * with the content of the plugin's implementation class such as
     * {@code net.nunoachenriques.empa.plugin.vadersentimentanalysis.TextSentimentDetector}).
     * Loads the first one to be found.
     *
     * @param c A Java class name of a specific implementation to pick, {@code null} for auto discovery.
     * @throws ExceptionInInitializerError On failure to get a service implementation!
     */
    public static synchronized void setAvailableImplementation(Class c)
            throws ExceptionInInitializerError {
        implementation = null;
        try {
            loader.reload();
            if (c == null) {
                implementation = loader.iterator().next();
            } else {
                for (HumanSentimentIdentifier hli : loader) {
                    if (c.equals(hli.getClass())) {
                        implementation = hli;
                        break;
                    }
                }
            }
            Logger.debug("Implementation: [{}]", implementation.toString());
        } catch (Exception e) {
            Logger.error("Implementation not loaded! {}", e.getMessage());
            throw new ExceptionInInitializerError(e);
        }
    }

    /**
     * @see HumanSentimentIdentifier#getAvailableLanguages()
     */
    public List<String> getAvailableLanguages() {
        return implementation.getAvailableLanguages();
    }

    /**
     * @see HumanSentimentIdentifier#getModel()
     */
    public HumanSentimentModel getModel() {
        return implementation.getModel();
    }

    /**
     * @see HumanSentimentIdentifier#getSentiment(java.lang.String, java.lang.String)
     */
    public Float getSentiment(String sample, String language) {
        Float s = implementation.getSentiment(sample, language);
        if (s == null || s < -1.0 || s > 1.0) {
            Logger.error("Sentiment value [{}] out of bounds [-1, 1]! Hint: check plugin implementation!", s);
            s = null;
        }
        return s;
    }

    /**
     * @see HumanSentimentIdentifier#getSentimentByClass(java.lang.String, java.lang.String)
     */
    public Map<HumanSentimentModel, Float> getSentimentByClass(String sample, String language) {
        Map<HumanSentimentModel, Float> sm = implementation.getSentimentByClass(sample, language);
        for (Map.Entry<HumanSentimentModel, Float> m : sm.entrySet()) {
            Float v = m.getValue();
            if (v == null || v < 0.0 || v > 1.0) {
                Logger.error("Sentiment value [{}] out of class [{}] bounds [0, 1]! Hint: check plugin implementation!", v, m.getKey());
                sm = null;
            }
        }
        return sm;
    }
}
