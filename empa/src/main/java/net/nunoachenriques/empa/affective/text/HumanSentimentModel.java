/* 
 * Copyright 2017 Nuno A. C. Henriques [nunoachenriques.net]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.nunoachenriques.empa.affective.text;

/**
 * Human sentiment analysis supported models and classes/axes by name.
 *
 * @author Nuno A. C. Henriques [nunoachenriques.net]
 */
public interface HumanSentimentModel {

    /**
     * Available models.
     */
    enum MODEL
            implements HumanSentimentModel {
        POLARITY, VAD
    }

    /**
     * Polarity model with three classes: NEGATIVE, NEUTRAL, POSITIVE.
     */
    enum POLARITY
            implements HumanSentimentModel {
        NEGATIVE, NEUTRAL, POSITIVE
    }

    /**
     * Valence, Arousal, Dominance model with three classes/axes:
     */
    enum VAD
            implements HumanSentimentModel {
        VALENCE, AROUSAL, DOMINANCE
    }
}
