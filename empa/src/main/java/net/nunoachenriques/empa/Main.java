/*
 * Copyright 2017 Nuno A. C. Henriques [nunoachenriques.net]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.nunoachenriques.empa;

/**
 * The class with a static main to run this project as an application
 * instead of using it only as a library.
 *
 * @author Nuno A. C. Henriques [nunoachenriques.net]
 */
public final class Main {

    /**
     * The running main so this project may be executable instead of being only
     * a library.
     *
     * @param args Default arguments!
     */
    public static void main(String[] args) {
        // TODO ... server!
    }
}
