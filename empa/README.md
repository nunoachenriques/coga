[![Apache 2.0 License](https://img.shields.io/badge/license-Apache%202.0-blue.svg "Apache 2.0 License")][1]

# EMPA --- Empathetic agent

An artificial intelligence (AI) companion to humans. The EMPA package
delivers affective, natural language and other useful libraries to
support an AI towards empathetic interaction.

## Repository

https://gitlab.com/nunoachenriques/coga/tree/master/empa

## Library

`net.nunoachenriques.empa`

The library is architectured and engineered in a plugin style, i.e.,
some small modules (subpackages) define a service interface and
provider. Thus, depending on the offered implementations by the
plugins. This way several different implementations may be easily tested.
An example for the `net.nunoachenriques.empa.language`:

1. It defines the `HumanLanguageIdentifier` service interface and the
   `HumanLanguageIdentifierService` provider.

2. It depends on the distributed default plugin
([Optimaize Language Detector EMPA Plugin](https://gitlab.com/nunoachenriques/coga/tree/master/empa-plugin-optimaizelanguagedetector))

3. The default plugin announces itself with the text
`net.nunoachenriques.empa.plugin.optimaizelanguagedetector.TextLanguageDetector`
in the file
`META-INF/services/net.nunoachenriques.empa.language.HumanLanguageIdentifier`

### Affective

`net.nunoachenriques.empa.affective`

#### Text

`net.nunoachenriques.empa.affective.text`

Default plugin
([VADER Sentiment Analysis EMPA Plugin](https://gitlab.com/nunoachenriques/coga/tree/master/empa-plugin-vadersentimentanalysis))

* _`HumanSentimentIdentifier`_
* `HumanSentimentIdentifierService`
* _`HumanSentimentModel`_

#### Face

`net.nunoachenriques.empa.affective.face`

**TODO**

#### Speech

`net.nunoachenriques.empa.affective.speech`

**TODO**

### Empathy

`net.nunoachenriques.empa.empathy`

**TODO**

### Impact

`net.nunoachenriques.empa.impact`

**TODO**

### Language

`net.nunoachenriques.empa.language`

Default plugin
([Optimaize Language Detector EMPA Plugin](https://gitlab.com/nunoachenriques/coga/tree/master/empa-plugin-optimaizelanguagedetector))

* _`HumanLanguageIdentifier`_
* `HumanLanguageIdentifierService`

Default plugin
([Apertium Language Translator EMPA Plugin](https://gitlab.com/nunoachenriques/coga/tree/master/empa-plugin-apertiumlanguagetranslator))

* _`HumanLanguageTranslate`_
* `HumanLanguageTranslateService`

## Install

Refer to the root project
[CogA](https://gitlab.com/nunoachenriques/coga/).

## License

Copyright 2017 Nuno A. C. Henriques [http://nunoachenriques.net/]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

[1]: http://www.apache.org/licenses/LICENSE-2.0.html
