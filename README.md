[![Apache 2.0 License](https://img.shields.io/badge/license-Apache%202.0-blue.svg "Apache 2.0 License")][1]

# CogA --- Cognitive and affective library

CogA is a cognitive and affective library set in Java. It's initial 
purpose is to support the SensAI Android app for humans to be able to
interact with an empathetic artificial agent (EMPA) using mobile
devices. It's incomplete, check https://gitlab.com/nunoachenriques/coga/tree/master/empa
for what's already working.

## Repository

https://gitlab.com/nunoachenriques/coga

## Gradle

This project includes the [Gradle](https://gradle.org/) wrapper for the common
PC operating systems Linux and macOS (`gradlew`) and Windows (`gradlew.bat`).
The included build tool (e.g., compiling, testing, releasing) should be used
with no need for further dependencies. Example for the `test` task:

 * All (EMPA and EMPA plugins): `./gradlew test`
 * Single (prefix `test` with `:name_of_the_project:`): `./gradlew :empa:test`

## Test

```shell
./gradlew test
```

The reports from test results may be checked at
`build/reports/tests/test/index.html` of each subproject (e.g., EMPA:
**`empa/build/reports/tests/test/index.html`**).

## Release

```shell
./gradlew release
```

The released ZIP files (binary, sources, javadoc) are at **`empa/build/distributions`**.

## Documentation

```shell
./gradlew javadoc
```

The released documentation is at `build/docs/javadoc` of each subproject
(e.g., EMPA: **`empa/build/docs/javadoc`**).

## Install

This Gradle (root) build file does all the tasks for the subprojects. EMPA is 
the empathetic agent library, every subproject directory name prefixed with 
`empa-plugin-` is a plugin for the EMPA library.

```shell
./gradlew :empa:installDist
```

The released packages for distribution are at **`empa/build/install/empa`**.

There's **no need** to run the build for each plugin.
Although, it's possible to do it (e.g., `./gradlew 
:empa-plugin-vadersentimentanalysis:installDist`) as also single testing.

## Troubleshooting

### Java 1.7 compatibility

1. Install OpenJDK 7.
2. Suffix Gradle command-line with `-Dorg.gradle.java.home=/path_to_jdk_7` such as (Debian GNU/Linux):

```shell
./gradlew :empa:installDist -Dorg.gradle.java.home=/usr/lib/jvm/java-7-openjdk-amd64/
```

## License

Copyright 2017 Nuno A. C. Henriques [http://nunoachenriques.net/]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

[1]: http://www.apache.org/licenses/LICENSE-2.0.html
