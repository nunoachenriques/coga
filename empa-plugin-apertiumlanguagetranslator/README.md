[![Apache 2.0 License](https://img.shields.io/badge/license-Apache%202.0-blue.svg "Apache 2.0 License")][1]

# Apertium Language Translator EMPA Plugin

## Repository

https://gitlab.com/nunoachenriques/coga/tree/master/empa-plugin-apertiumlanguagetranslator

## Plugin

`net.nunoachenriques.empa.plugin.apertiumlanguagetranslator`

Written text human language translator plugin for
[EMPA](https://gitlab.com/nunoachenriques/coga/tree/master/empa) using
[Apertium Language Translator](https://github.com/nunoachenriques/lttoolbox-java)
application.

The plugin announces itself with the text
`net.nunoachenriques.empa.plugin.apertiumlanguagetranslator.TextLanguageTranslator`
in the file
`META-INF/services/net.nunoachenriques.empa.language.HumanLanguageTranslate`

## Install

Refer to the root project
[CogA](https://gitlab.com/nunoachenriques/coga/).

## Use cases

### As an [EMPA](https://gitlab.com/nunoachenriques/coga/tree/master/empa) plugin

The
[Apertium Language Translator EMPA Plugin](https://gitlab.com/nunoachenriques/coga/tree/master/empa-plugin-apertiumlanguagetranslator)
distribution JAR must be in the classpath and has to be loaded by a
service provider class (as a `HumanLanguageTranslate` implementation).

```java
HumanLanguageTranslateService hlts = HumanLanguageTranslateService.getInstance();
hlts.setup(new File(System.getProperty("java.io.tmpdir")));
String t1 = hlts.getTranslation("hello", "en", "gl");
String t2 = hlts.getTranslation("ola", "gl", "en");
```

### Stand alone

It may be used integrated with any other application class.

```java
import net.nunoachenriques.empa.plugin.apertiumlanguagetranslator.TextLanguageTranslator
...
TextLanguageTranslator tlt = new TextLanguageTranslator();
tlt.setup(new File(System.getProperty("java.io.tmpdir")));
String t1 = tlt.getTranslation("hello", "en", "gl");
String t2 = tlt.getTranslation("ola", "gl", "en");
```

## [Apertium project](https://www.apertium.org/)

[Built language packages](https://sourceforge.net/p/apertium/svn/HEAD/tree/builds/)
downloaded from the current HEAD [r84368](https://sourceforge.net/p/apertium/svn/84368/).

 * apertium-en-gl

   * From: https://sourceforge.net/p/apertium/svn/HEAD/tree/builds/apertium-en-gl/apertium-en-gl.jar | 2016-02-22 | r65773
   * Local: `src/main/resources/apertium-en-gl.jar` (`org/` removed)

 * apertium-pt-gl

   * From: https://sourceforge.net/p/apertium/svn/HEAD/tree/builds/apertium-pt-gl/apertium-pt-gl.jar | 2016-02-22 | r65773
   * Local: `src/main/resources/apertium-pt-gl.jar` (`org/` removed)

## License

Copyright 2018 Nuno A. C. Henriques [http://nunoachenriques.net/]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

[1]: http://www.apache.org/licenses/LICENSE-2.0.html
