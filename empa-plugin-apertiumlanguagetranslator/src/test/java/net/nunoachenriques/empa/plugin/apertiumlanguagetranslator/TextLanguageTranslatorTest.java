/* 
 * Copyright 2018 Nuno A. C. Henriques [nunoachenriques.net]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.nunoachenriques.empa.plugin.apertiumlanguagetranslator;

import junitparams.JUnitParamsRunner;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.pmw.tinylog.Configurator;
import org.pmw.tinylog.Level;

import java.io.File;
import java.util.*;

import static org.junit.Assert.*;

/**
 * Testing the text language translator functionality!
 * 
 * @author Nuno A. C. Henriques [nunoachenriques.net]
 */
@RunWith(JUnitParamsRunner.class)
public class TextLanguageTranslatorTest {

    private TextLanguageTranslator tlt;

    /**
     * Instantiates local <code>TextLanguageTranslator</code> object!
     */
    @Before
    public void testTextLanguageTranslator() throws Exception {
        Configurator.defaultConfig().level(Level.DEBUG).activate();
        tlt = new TextLanguageTranslator();
        tlt.setup(new File(System.getProperty("java.io.tmpdir")));
        assertNotNull("No TextLanguageTranslator instantiated!", tlt);
    }

    /**
     * Tests the default four language pairs available: en-gl, gl-en,
     * gl-pt, pt-gl.
     */
    @Test
    public void testGetAvailableLanguagePairs() {
        List<String> defaultAlp = Arrays.asList("en-gl", "gl-en", "gl-pt", "pt-gl");
        List<Map.Entry<String, String>> alp = tlt.getAvailableLanguagePairs();
        for (Map.Entry<String, String> p : alp) {
            String s = p.getKey() + "-" + p.getValue();
            assertTrue(s + " NOT AVAILABLE!", defaultAlp.contains(s));
        }
    }

    /**
     * Tests if it translates!
     */
    @Test
    public void testGetTranslation() {
        try {
            assertEquals("Translation FAILED! pt-gl Olá -> Ola", "Ola", tlt.getTranslation("Olá", "pt", "gl"));
            assertEquals("Translation FAILED! en-gl Hello -> Ola", "Ola", tlt.getTranslation("Hello", "en", "gl"));
        } catch (Exception e) {
            fail();
        }
    }
}
