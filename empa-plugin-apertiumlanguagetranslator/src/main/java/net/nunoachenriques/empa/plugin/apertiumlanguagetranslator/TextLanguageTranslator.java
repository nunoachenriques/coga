/* 
 * Copyright 2018 Nuno A. C. Henriques [nunoachenriques.net]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.nunoachenriques.empa.plugin.apertiumlanguagetranslator;

import net.nunoachenriques.empa.language.HumanLanguageTranslate;
import org.apache.commons.io.FileUtils;
import org.apertium.Translator;
import org.pmw.tinylog.Logger;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.*;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

/**
 * Implements the
 * {@link net.nunoachenriques.empa.language.HumanLanguageTranslate}
 * functionality, i.e., it's a plugin! It translates a text sample from one
 * language to another. <strong>Use case</strong>:
 * {@link #TextLanguageTranslator()}. Not thread-safe, should be guaranteed by
 * the service loader if needed. Some code adapted from:
 * <a href="https://github.com/nunoachenriques/apertium-android">Apertium for Android</a>
 *
 * <p><strong>Use case</strong>:</p>
 * <pre>
 * ...
 * <code>
 * import net.nunoachenriques.empa.plugin.apertiumlanguagetranslator.TextLanguageTranslator
 * </code>
 * ...
 * <code>
 * TextLanguageTranslator tlt = new TextLanguageTranslator();
 * tlt.setup(new File(System.getProperty("java.io.tmpdir")));
 * String t1 = tlt.getTranslation("hello", "en", "gl");
 * String t2 = tlt.getTranslation("ola", "gl", "en");
 * </code>
 * ...
 * </pre>
 *
 * @author Nuno A. C. Henriques [nunoachenriques.net]
 */
public final class TextLanguageTranslator
        implements HumanLanguageTranslate {

    private static final List<String> APERTIUM_PKG = new ArrayList<String>() {{
        add("apertium-en-gl");
        add("apertium-pt-gl");
    }};
    private static final FilenameFilter APERTIUM_PKG_FILTER = new FilenameFilter() {
        @Override
        public boolean accept(File dir, String name) {
            return name.matches("apertium-[a-z][a-z][a-z]?-[a-z][a-z][a-z]?");
        }
    };
    /*
     * Adapted from: ApertiumInstallation (https://github.com/nunoachenriques/apertium-android)
     */
    private final Map<String, String> modeToPackage = new HashMap<>();

    private File apertiumCodeCacheDir;
    private File apertiumInstallJarDir;
    private File apertiumInstallPackageDir;

    /**
     * Default empty constructor.
     *
     * @see #setup(File)
     */
    @SuppressWarnings("WeakerAccess")
    public TextLanguageTranslator() {
    }

    @Override
    public void setup(File baseDir)
            throws Exception {
        if (initCacheRepository(baseDir)) {
            installLanguagePackages();
            loadInstalledLanguagePackages();
        } else {
            throw new IOException("Unable to get resources and create directories [" + baseDir.toString() + "]. Hint: filesystem permissions?");
        }
    }

    @Override
    public List<Map.Entry<String, String>> getAvailableLanguagePairs() {
        String[] modes = Translator.getAvailableModes();
        List<Map.Entry<String, String>> pairs = new ArrayList<>(modes.length);
        for (String m : modes) {
            String[] s = m.split("-");
            pairs.add(new AbstractMap.SimpleImmutableEntry<>(s[0], s[1]));
        }
        return pairs;
    }

    @Override
    public String getTranslation(String sample, String from, String to)
            throws Exception {
        ClassLoader cl = Thread.currentThread().getContextClassLoader();
        try { // Android Dalvik VM
            String dexPath = new File(apertiumInstallJarDir, modeToPackage.get(from + "-" + to) + ".jar").toString();
            ClassLoader dexClassLoader = (ClassLoader) cl.loadClass("dalvik.system.DexClassLoader").getConstructor(String.class, String.class, String.class, ClassLoader.class).newInstance(dexPath, apertiumCodeCacheDir.toString(), null, cl);
            Logger.debug("Dalvik VM");
            return getTranslation(sample, from, to, dexClassLoader);
        } catch (ClassNotFoundException | NoSuchMethodException e) { // Java VM
            Logger.debug("Java VM");
            return getTranslation(sample, from, to, cl);
        }
    }

    private String getTranslation(String sample, String from, String to, ClassLoader cl)
            throws Exception {
        String mode = from + "-" + to;
        String pkg = modeToPackage.get(mode);
        Translator.setBase(getLanguagePackageDir(pkg), cl);
        Translator.setMode(mode);
        return Translator.translate(sample);
    }

    private boolean initCacheRepository(File baseDir) {
        apertiumCodeCacheDir = new File(baseDir, "code");
        apertiumInstallJarDir = new File(baseDir, "jar");
        apertiumInstallPackageDir = new File(baseDir, "package");
        return (apertiumCodeCacheDir.isDirectory() && apertiumInstallJarDir.isDirectory() && apertiumInstallPackageDir.isDirectory()
                || (apertiumCodeCacheDir.mkdirs() && apertiumInstallJarDir.mkdirs() && apertiumInstallPackageDir.mkdirs()));
    }

    private void installLanguagePackages()
            throws IOException {
        // Available language packages
        for (String ap : APERTIUM_PKG) {
            File apDir = new File(apertiumInstallPackageDir, ap);
            if (!apDir.exists()) { // Installs ONLY if missing
                // Copy resource JAR to file at install dir
                String apJarResource = ap + ".jar";
                File apJarFile = new File(apertiumInstallJarDir, apJarResource);
                FileUtils.copyInputStreamToFile(getClass().getClassLoader().getResourceAsStream(apJarResource), apJarFile);
                // Install all files from JAR file to a specific (package) dir
                if (apDir.mkdirs()) {
                    try (JarFile jf = new JarFile(apJarFile, false)) {
                        Enumeration<JarEntry> apJarEntries = jf.entries();
                        while (apJarEntries.hasMoreElements()) {
                            JarEntry je = apJarEntries.nextElement();
                            String jeName = je.getName();
                            File jeFile = new File(apDir, jeName);
                            if (!je.isDirectory()) {
                                FileUtils.copyInputStreamToFile(jf.getInputStream(je), jeFile);
                            }
                        }
                    }
                } else {
                    throw new IOException("Unable to create directories [" + apDir.toString() + "]. Hint: filesystem permissions?");
                }
            }
        }
    }

    /*
     * Adapted from: ApertiumInstallation (https://github.com/nunoachenriques/apertium-android)
     */
    private void loadInstalledLanguagePackages()
            throws Exception {
        String[] installedPackages = apertiumInstallPackageDir.list(APERTIUM_PKG_FILTER);
        Logger.info("Language packages: " + Arrays.toString(installedPackages));
        if (installedPackages != null) {
            for (String pkg : installedPackages) {
                String pkgPath = getLanguagePackageDir(pkg);
                Translator.setBase(pkgPath, null);
                for (String mode : Translator.getAvailableModes()) {
                    Logger.info("mode: " + mode + " | pkgPath: " + pkgPath);
                    modeToPackage.put(mode, pkg);
                }
            }
        } else {
            Logger.warn("NO languages INSTALLED!");
        }
    }

    /*
     * Adapted from: ApertiumInstallation (https://github.com/nunoachenriques/apertium-android)
     */
    private String getLanguagePackageDir(String pkg) {
        return new File(apertiumInstallPackageDir, pkg).toString();
    }
}
